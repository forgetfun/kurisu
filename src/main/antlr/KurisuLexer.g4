lexer grammar KurisuLexer;

// Common fragments

fragment DECIMAL_DIGIT
	: [0-9] ;

fragment DECIMAL_DIGIT_NONZERO
	: [1-9] ;

// Whitespace

NEWLINE: '\r\n' | 'r' | '\n' ;
WHITESPACE
	: [\t ]+
	-> skip ;

// Literals

LITERAL_NUMBER_INTEGER
	: '0'
	| DECIMAL_DIGIT_NONZERO DECIMAL_DIGIT* ;
LITERAL_NUMBER_DECIMAL
	: DECIMAL_DIGIT_NONZERO DECIMAL_DIGIT* '.' DECIMAL_DIGIT+
	| '0.' DECIMAL_DIGIT+ ;

// Operators

OPERATOR_ADDITION           : '+' ;
OPERATOR_SUBTRACTION        : '-' ;
OPERATOR_MULTIPLICATION     : '*' ;
OPERATOR_DIVISION           : '/' ;
OPERATOR_ASSIGNMENT         : '=' ;

PARENTHESIS_LEFT : '(' ;
PARENTHESIS_RIGHT: ')' ;

// Delimiters

COMA: ',' ;

// Identifiers
//ID                 : [_]*[a-z][A-Za-z0-9_]* ;
