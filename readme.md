
# Kurisu programming language

Kurisu is experimental programming language which is heavily expression-oriented and strives to introduce as few keywords and special syntax as possible while retaining readibility and simplicity
