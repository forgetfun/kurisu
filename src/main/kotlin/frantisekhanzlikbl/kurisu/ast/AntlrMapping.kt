package frantisekhanzlikbl.kurisu.ast

import frantisekhanzlikbl.kurisu.antlr.generated.KurisuLexer
import frantisekhanzlikbl.kurisu.antlr.generated.KurisuParser
import frantisekhanzlikbl.kurisu.ast.node.BinaryOperation
import frantisekhanzlikbl.kurisu.ast.node.Expression
import frantisekhanzlikbl.kurisu.ast.node.File
import frantisekhanzlikbl.kurisu.ast.node.Number
import frantisekhanzlikbl.kurisu.ast.node.UnaryOperation

private fun reportUnexpectedContext(name: String? = "?"): Nothing = throw IllegalArgumentException("Unexpected ANTLR context found: `${name ?: "?"}`")
private fun reportUnexpectedToken(type: Int): Nothing = throw IllegalArgumentException("Unexpected ANTLR token found: `${KurisuLexer.VOCABULARY.getSymbolicName(type) ?: "?"}`")
fun KurisuParser.FileContext.toAstNode() = File(this.expressions.map(KurisuParser.ExpressionContext::toAstNode))
private fun KurisuParser.ExpressionContext.toAstNode(): Expression = when (this) {
	is KurisuParser.ExpressionOperationBinaryContext -> this.toAstNode()
	is KurisuParser.ExpressionParenthesisContext -> this.expression().toAstNode()
	is KurisuParser.ExpressionNumberLiteralContext -> this.toAstNode()
	is KurisuParser.ExpressionOperationUnaryContext -> this.toAstNode()
	else -> reportUnexpectedContext(this.javaClass.simpleName)
}

private fun KurisuParser.ExpressionOperationBinaryContext.toAstNode(): BinaryOperation = when (this.operator.type) {
	KurisuLexer.OPERATOR_ADDITION -> BinaryOperation.Addition(this.operandLeft.toAstNode(), this.operandRight.toAstNode())
	KurisuLexer.OPERATOR_SUBTRACTION -> BinaryOperation.Subtraction(this.operandLeft.toAstNode(), this.operandRight.toAstNode())
	KurisuLexer.OPERATOR_MULTIPLICATION -> BinaryOperation.Multiplication(this.operandLeft.toAstNode(), this.operandRight.toAstNode())
	KurisuLexer.OPERATOR_DIVISION -> BinaryOperation.Division(this.operandLeft.toAstNode(), this.operandRight.toAstNode())
	else -> reportUnexpectedToken(this.operator.type)
}

private fun KurisuParser.ExpressionNumberLiteralContext.toAstNode() = this.numberLiteral().toAstNode()
private fun KurisuParser.NumberLiteralContext.toAstNode() = when (this) {
	is KurisuParser.NumberLiteralIntegerContext -> this.toAstNode()
	is KurisuParser.NumberLiteralDecimalContext -> this.toAstNode()
	else -> reportUnexpectedContext(this.javaClass.simpleName)
}

private fun KurisuParser.NumberLiteralIntegerContext.toAstNode() = Number.Integer(this.LITERAL_NUMBER_INTEGER().text.toInt())
private fun KurisuParser.NumberLiteralDecimalContext.toAstNode() = Number.Decimal(this.LITERAL_NUMBER_DECIMAL().text.toFloat())
private fun KurisuParser.ExpressionOperationUnaryContext.toAstNode() = when (this.operator.type) {
	KurisuLexer.OPERATOR_SUBTRACTION -> UnaryOperation.Minus(this.operand.toAstNode())
	else -> reportUnexpectedToken(this.operator.type)
}
