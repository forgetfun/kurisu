package frantisekhanzlikbl.kurisu.utils.parser.structure

interface BranchNode : Node {
	val children: List<Node>
}

data class SimpleBranchNode(override val name: String, override val children: List<Node>) : BranchNode
